const express = require('express')
const app = express()
const bodyParser = require('body-parser')
require('dotenv').config()
const session = require('express-session')
const cors = require('cors')

const sessionStore = require('express-mysql-session')(session)

const jobs = require('./functions/jobs')
const auth = require('./functions/authentication')
const users = require('./functions/users')
const testimonials = require('./functions/testimonials')
const { connection } = require('./functions/commonFunctions')

app.use(
  cors({
    credentials: true,
    exposedHeaders: ['set-cookie'],
    origin: [
      'http://localhost:8000',
      'http://localhost:9000',
      'http://localhost:8001'
    ],
  }),
)
app.use(
  bodyParser.json({
    limit: '50mb',
  }),
)

app.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    store: new sessionStore({}, connection),
  }),
)

// AUTHENTICATION
app.post('/cms/login', auth.login)
app.post('/cms/verify', auth.verify)
app.post('/cms/reset-password', auth.resetPassword)
app.post('/cms/verify-link', auth.verifyPasswordLink)
app.post('/cms/change-password', auth.changePassword)
app.get('/cms/get-role', auth.getRole)

// JOBS
app.post('/cms/jobs/create', jobs.createJob)
app.post('/cms/jobs/delete', jobs.deleteJob)
app.post('/cms/jobs/edit', jobs.updateJob)
app.post('/cms/jobs/create-draft', jobs.addDraft)
app.get('/jobs/clear-draft', jobs.deleteDraft)
app.patch('/cms/jobs/delete-drafts', jobs.deleteAllDrafts)
app.post('/cms/jobs', jobs.getJobsCMS)
app.get('/jobs', jobs.getJobs)

// USERS
app.get('/cms/users', users.getUsers)
app.post('/cms/users/create', users.createUser)
app.post('/cms/users/delete', users.deleteUser)
app.post('/cms/users/edit', users.updateUser)

// PROFILE
app.get('/cms/profile/', users.getProfile)

// TESTIMONIALS
app.get('/cms/testimonials/', testimonials.getTestimonials)
app.get('/testimonials/', testimonials.getPublicTestimonials)
app.post('/cms/testimonials/add', testimonials.createTestimonial)
app.post('/cms/testimonials/edit', testimonials.editTestimonial)
app.post('/cms/testimonials/delete', testimonials.deleteTestimonial)
app.post('/cms/testimonials/create-draft', testimonials.addTestimonialDraft)
app.get('/testimonials/clear-draft', testimonials.deleteTestimonialDraft)
app.patch(
  '/cms/testimonials/delete-drafts',
  testimonials.deleteAllTestimonialDrafts,
)

app.listen(8080, () => console.log(`Listening on port 8080!`))
