require('dotenv').config()

const crypto = require('crypto')
const { connection, checkAuth } = require('./commonFunctions')

// CREATE JOB
exports.createJob = async (req, res) => {
  const {
    position,
    shortDescription,
    location,
    bambooHR,
    department,
  } = req.body

  if (
    !position ||
    !shortDescription ||
    !location ||
    !bambooHR ||
    !department ||
    !req.body.jwt
  ) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')

  await connection.query(
    `INSERT INTO jobs (jobTitle, jobDescription, jobLocation, jobLink, jobCategory) VALUES (?,?,?,?,?)`,
    [position, shortDescription, location, bambooHR, department],
  )
  res.sendStatus(200)
}

// DELETE JOB
exports.deleteJob = async (req, res) => {
  const { id } = req.body

  if (!id || !req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')

  await connection.query(`DELETE FROM jobs WHERE jobID=?`, [id])
  res.sendStatus(200)
}

// UPDATE JOB
exports.updateJob = async (req, res) => {
  const {
    position,
    shortDescription,
    location,
    bambooHR,
    department,
    id,
  } = req.body

  if (
    !position ||
    !shortDescription ||
    !location ||
    !bambooHR ||
    !department ||
    !id ||
    !req.body.jwt
  ) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')

  await connection.query(
    `UPDATE jobs SET jobTitle=?, jobDescription=?, jobLocation=?, jobLink=?, jobCategory=? WHERE jobID=?`,
    [position, shortDescription, location, bambooHR, department, id],
  )
  res.sendStatus(200)
}

// ADD DRAFT
exports.addDraft = async (req, res) => {
  const { location, id } = req.body

  if (!location || !req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')

  const tempPreview = crypto.randomBytes(25).toString('hex')
  if (id) connection.query(`UPDATE jobs SET editing=? WHERE jobID=?`, [id, id])
  await connection.query(
    `INSERT INTO jobs (jobTitle, jobDescription, jobLocation, jobLink, jobCategory, draft,editing) VALUES (?,?,?,?,?,?,?)`,
    [
      req.body.position,
      req.body.shortDescription,
      location,
      req.body.bambooHR,
      req.body.department,
      tempPreview,
      id,
    ],
  )
  res.send(tempPreview)
}

// DELETE DRAFT
exports.deleteDraft = async (req, res) => {
  const [
    data,
  ] = await connection.query(`SELECT editing FROM jobs WHERE draft=?`, [
    req.query.preview,
  ])
  if (res.length && res[0].editing)
    connection.query(`UPDATE jobs SET editing=NULL WHERE editing=?`, [
      res[0].editing,
    ])
  connection.query(`DELETE FROM jobs WHERE draft=?`, [req.query.preview])
}

// DELETE ALL DRAFTS
exports.deleteAllDrafts = async (req, res) => {
  await connection.query(`DELETE FROM jobs WHERE draft IS NOT NULL`)
  await connection.query(
    `UPDATE jobs SET editing=NULL WHERE editing IS NOT NULL`,
  )
  res.sendStatus(200)
}

// GET CMS
exports.getJobsCMS = async (req, res) => {
  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')

  // Check if user has permissions to access
  const [data] = await connection.query(
    `SELECT * FROM jobs WHERE draft IS NULL`,
  )
  res.send(data)
}

// GET PUBLIC
exports.getJobs = async (req, res) => {
  const [
    data,
  ] = await connection.query(
    `SELECT jobTitle, jobDescription, jobLink, jobCategory, jobLocation, draft FROM jobs WHERE draft IS NULL ${
      req.query.preview ? 'AND editing IS NULL OR draft=?' : ''
    }`,
    [req.query.preview],
  )
  res.send(data)
}
