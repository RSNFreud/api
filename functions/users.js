require('dotenv').config()

const jwt = require('jsonwebtoken')
const cloudinary = require('cloudinary').v2
const bcrypt = require('bcrypt')
const { connection, checkAuth } = require('./commonFunctions')

// CREATE USER
exports.createUser = async (req, res) => {
  const {
    username,
    email,
    password,
    confirmPassword,
    role,
    jobTitle,
  } = req.body

  if (
    !username ||
    !email ||
    !password ||
    !confirmPassword ||
    !role ||
    !jobTitle ||
    !req.body.jwt
  ) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Admin')))
    return res.status(401).send('Token invalid!')

  const profilePicture = req.body.profilePic

  if (password != confirmPassword)
    return res.status(409).send('Passwords do not match!')

  bcrypt.hash(
    password,
    Math.floor(Math.random() * 9) + 1,
    async (e, password) => {
      try {
        const [
          data,
        ] = await connection.query(
          `INSERT INTO users (email, password, name, position, role) VALUES (?,?,?,?,?)`,
          [email, password, username, jobTitle, role],
        )
        if (profilePicture) {
          cloudinary.uploader.upload(
            profilePicture,
            { folder: `profilePictures/` },
            async (error, result) => {
              const publicID = result.public_id
              const imageURL = result.url
              await connection.query(
                `UPDATE users SET profileURL=?, profilePublicID=? WHERE id=?`,
                [imageURL, publicID, data.insertId],
              )
              res.sendStatus(200)
            },
          )
        } else res.sendStatus(200)
      } catch {
        if (err) return res.status(409).send('This email exists already!')
      }
    },
  )
}

// DELETE USER
exports.deleteUser = async (req, res) => {
  const { id } = req.body

  if (!id || !req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Admin')))
    return res.status(401).send('Token invalid!')

  const [
    data,
  ] = await connection.query(`SELECT profilePublicID FROM users WHERE id=?`, [
    id,
  ])

  if (data[0].profilePublicID) {
    let publicID = data[0].profilePublicID
    cloudinary.uploader.destroy(publicID)
  }

  await connection.query(`DELETE FROM users WHERE id=?`, [id])
  res.sendStatus(200)
}

// UPDATE USER
exports.updateUser = async (req, res) => {
  const {
    username,
    password,
    confirmPassword,
    role,
    jobTitle,
    id,
    profilePic,
  } = req.body

  if (!username || !role || !id || !jobTitle || !req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Admin')))
    return res.status(401).send('Token invalid!')

  connection.query(`UPDATE users SET name=?, position=?, role=? WHERE id=?`, [
    username,
    jobTitle,
    role,
    id,
  ])

  const [
    data,
  ] = await connection.query(
    `SELECT profilePublicID, profileURL FROM users WHERE id=?`,
    [id],
  )

  // If the picture hasnt changed
  if (data[0].profileURL === profilePic) {
    if (!password) res.sendStatus(200)
    return
  }
  if (profilePic && profilePic.includes('cloudinary')) {
    if (!password) res.sendStatus(200)
    return
  }

  // Delete the original picture
  if (data[0].profilePublicID) {
    let publicID = data[0].profilePublicID
    await connection.query(
      `UPDATE users SET profileURL=NULL, profilePublicID=NULL WHERE id=?`,
      [id],
    )
    !profilePic && !password && res.sendStatus(200)
    cloudinary.uploader.destroy(publicID)
  }

  // Upload new picture
  if (profilePic)
    cloudinary.uploader.upload(
      profilePic,
      { folder: `profilePictures/` },
      async (error, result) => {
        const publicID = result.public_id
        const imageURL = result.url
        await connection.query(
          `UPDATE users SET profileURL=?, profilePublicID=? WHERE id=?`,
          [imageURL, publicID, id],
        )
      },
      !password && res.sendStatus(200),
    )

  if (password && password != confirmPassword)
    return res.status(409).send('Passwords do not match!')
  if (password)
    bcrypt.hash(
      password,
      Math.floor(Math.random() * 9) + 1,
      async (e, password) => {
        await connection.query(`UPDATE users SET password=? WHERE id=?`, [
          password,
          id,
        ])
      },
      res.sendStatus(200),
    )
}

// GET USERS
exports.getUsers = async (req, res) => {
  if (!req.query.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Admin')))
    return res.status(401).send('Token invalid!')

  const [data] = await connection.query(`SELECT * FROM users`)
  res.send(data)
}

// GET PROFILE
exports.getProfile = async (req, res) => {
  if (!req.query.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')

  const token = req.body.jwt || req.query.jwt

  const id = jwt.decode(token).id
  const [
    data,
  ] = await connection.query(
    `SELECT name, position, profileURL, role FROM users WHERE id=?`,
    [id],
  )
  res.send(data)
}
