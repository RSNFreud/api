const nodemailer = require('nodemailer')
const jwt = require('jsonwebtoken')

exports.transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: process.env.MAIL,
    pass: process.env.MAIL_PASSWORD,
  },
})

const mysql = require('mysql2')

const connection = mysql
  .createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    socketPath: process.env.socketPath,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    database: process.env.DB,
  })
  .promise()

exports.connection = connection

exports.checkAuth = (req, roleRequired) => {
  return new Promise((res, rej) => {
    const token = req.body.jwt || req.query.jwt
    if (!token) return rej()

    jwt.verify(token, process.env.SECRET_KEY, async (err, decoded) => {
      if (err || (decoded && !decoded.id)) return rej()
      const [
        data,
      ] = await connection.query(`SELECT id, role from users WHERE id=?`, [
        decoded.id,
      ])
      if (!data.length) return rej()
      if (roleRequired) {
        if (data[0].role === 'Admin') return res(true)
        if (data[0].role !== roleRequired) return rej()
        else res(true)
      } else res(true)
    })
  }).catch((err) => {
    if (err) console.log(err)
  })
}
