require('dotenv').config()

const cloudinary = require('cloudinary').v2
const crypto = require('crypto')
const { connection, checkAuth } = require('./commonFunctions')

// GET TESTIMONIALS
exports.getTestimonials = async (req, res) => {
  if (!req.query.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Editor')))
    return res.status(401).send('Token invalid!')

  const [data] = await connection.query(
    `SELECT * FROM testimonials WHERE draft IS NULL`,
  )
  res.send(data)
}

// ADD TESTIMONIAL
exports.createTestimonial = async (req, res) => {
  const { name, position, description, profilePic } = req.body

  if (!position || !name || !description || !profilePic || !req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Editor')))
    return res.status(401).send('Token invalid!')

  cloudinary.uploader.upload(
    profilePic,
    { folder: `testimonials/` },
    async (error, result) => {
      if (!result) return res.status(400).send('Invalid image!')
      await connection.query(
        `INSERT INTO testimonials (name, quote, position, image, imagePublicID) VALUES (?,?,?,?,?)`,
        [name, description, position, result.url, result.public_id],
      )
      res.sendStatus(200)
    },
  )
}

// EDIT TESTIMONIAL
exports.editTestimonial = async (req, res) => {
  const { name, position, description, profilePic, id } = req.body

  if (
    !position ||
    !name ||
    !description ||
    !profilePic ||
    !id ||
    !req.body.jwt
  ) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Editor')))
    return res.status(401).send('Token invalid!')

  const [
    data,
  ] = await connection.query(
    `SELECT image, imagePublicID FROM testimonials WHERE testimonialID=?`,
    [id],
  )

  if (profilePic !== data[0].image) {
    cloudinary.uploader.destroy(data[0].imagePublicID)
    cloudinary.uploader.upload(
      profilePic,
      { folder: `testimonials/` },
      async (error, result) => {
        if (!result) return res.status(400).send('Invalid image!')
        await connection.query(
          `UPDATE testimonials SET name=?, quote=?, position=?, image=?, imagePublicID=? WHERE testimonialID=?`,
          [name, description, position, result.url, result.public_id, id],
        )
        res.sendStatus(200)
      },
    )
  } else {
    await connection.query(
      `UPDATE testimonials SET name=?, quote=?, position=? WHERE testimonialID=?`,
      [name, description, position, id],
    )
    res.sendStatus(200)
  }
}

// DELETE TESTIMONIAL
exports.deleteTestimonial = async (req, res) => {
  const { id } = req.body

  if (!id || !req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Editor')))
    return res.status(401).send('Token invalid!')

  // Check if user has permissions to access
  const [
    data,
  ] = await connection.query(
    `SELECT imagePublicID FROM testimonials WHERE testimonialID=?`,
    [id],
  )
  if (data[0].imagePublicID) cloudinary.uploader.destroy(data[0].imagePublicID)

  await connection.query(`DELETE FROM testimonials WHERE testimonialID=?`, [id])
  res.sendStatus(200)
}

// GET PUBLIC TESTIMONIALS
exports.getPublicTestimonials = async (req, res) => {
  const [
    data,
  ] = await connection.query(
    `SELECT name, quote, position, image FROM testimonials WHERE draft IS NULL ${
      req.query.preview ? 'AND editing IS NULL OR draft=?' : ''
    }`,
    [req.query.preview],
  )
  res.send(data)
}

// ADD DRAFT
exports.addTestimonialDraft = async (req, res) => {
  const { id, profilePic } = req.body

  if (!req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req, 'Editor')))
    return res.status(401).send('Token invalid!')

  const tempPreview = crypto.randomBytes(25).toString('hex')
  if (id)
    connection.query(`UPDATE testimonials SET editing=? WHERE jobID=?`, [
      id,
      id,
    ])

  const [
    data,
  ] = connection.query(
    `INSERT INTO testimonials (name, quote, position, draft,editing) VALUES (?,?,?,?,?)`,
    [req.body.name, req.body.description, req.body.position, tempPreview, id],
  )
  if (profilePic) {
    cloudinary.uploader.upload(
      profilePic,
      { folder: `testimonials/` },
      async (error, result) => {
        let newId = data.insertId
        await connection.query(
          `UPDATE testimonials SET image=?,imagePublicID=? WHERE testimonialID=?`,
          [result.url, result.public_id, newId],
        )
        res.send(tempPreview)
      },
    )
  } else res.send(tempPreview)
}

// DELETE DRAFT
exports.deleteTestimonialDraft = async (req, res) => {
  const [
    data,
  ] = connection.query(
    `SELECT editing, imagePublicID FROM testimonials WHERE draft=?`,
    [req.query.preview],
  )
  if (data.length && data[0].editing)
    connection.query(`UPDATE jobs SET editing=NULL WHERE editing=?`, [
      res[0].editing,
    ])

  if (data.length && data[0].imagePublicID) {
    let publicID = data[0].imagePublicID
    cloudinary.uploader.destroy(publicID)
  }
  connection.query(`DELETE FROM testimonials WHERE draft=?`, [
    req.query.preview,
  ])
}

// DELETE ALL DRAFTS
exports.deleteAllTestimonialDrafts = async (req, res) => {
  const [data] = await connection.query(
    `SELECT imagePublicID FROM testimonials WHERE draft IS NOT NULL`,
  )
  if (data.length && data[0].imagePublicID)
    cloudinary.uploader.destroy(data[0].imagePublicID)
  await connection.query(`DELETE FROM testimonials WHERE draft IS NOT NULL`)
  await connection.query(
    `UPDATE testimonials SET editing=NULL WHERE editing IS NOT NULL`,
  )
  res.sendStatus(200)
}
