require('dotenv').config()

const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const { connection, checkAuth, transporter } = require('./commonFunctions')

// LOGIN
exports.login = async (req, res) => {
  if (!req.body.email || !req.body.password) {
    return res.status(400).send('Missing required field!')
  }

  const { email, password } = req.body
  const [
    data,
  ] = await connection.query(
    `SELECT name,position,password,id FROM users WHERE email=?`,
    [email],
  )
  // Check email exists
  if (!data.length) return res.status(418).send('User not found!')

  // Check password matches
  bcrypt.compare(password, data[0].password, async (e, password) => {
    if (!password) return res.status(406).send('Invalid password')
    let timeOut = '24h'
    if (req.body.rememberMe) timeOut = '14d'
    await connection.query(`UPDATE users SET last_logged_in=? WHERE id=?`, [
      Date.now(),
      data[0].id,
    ])
    req.session.userID = data[0].id
    jwt.sign(
      {
        id: data[0].id,
      },
      process.env.SECRET_KEY,
      {
        expiresIn: timeOut,
      },
      (q, jwt) => {
        res.status(202).send(jwt)
      },
    )
  })
}

// GET ROLE
exports.getRole = (req, res) => {
  if (!req.query.jwt) {
    return res.status(400).send('Missing required field!')
  }

  const token = req.body.jwt || req.query.jwt

  jwt.verify(token, process.env.SECRET_KEY, async (err, decoded) => {
    if (err || !decoded.id) return res.status(401).send('Token invalid!')
    const [
      data,
    ] = await connection.query(`SELECT id, role from users WHERE id=?`, [
      decoded.id,
    ])
    if (!data.length) return res.status(401).send('Token invalid!')
    else res.send(data)
  })
}

// VERIFY JWT
exports.verify = async (req, res) => {
  console.log(req.session)
  if (!req.body.jwt) {
    return res.status(400).send('Missing required field!')
  }

  if (!(await checkAuth(req))) return res.status(401).send('Token invalid!')
  res.sendStatus(200)
}

// SEND PASSWORD RESET
exports.resetPassword = async (req, res) => {
  if (!req.body.email) return res.status(400).send('Missing required field!')
  const { email } = req.body
  const [
    data,
  ] = await connection.query(`SELECT email,id FROM users WHERE email=?`, [
    email,
  ])
  // Check email exists
  if (!data.length) return res.status(418).send('User not found!')
  let cryptoKey = crypto.randomBytes(25).toString('hex')
  // Send email
  res.sendStatus(200)
  transporter.sendMail({
    from: `<${process.env.MAIL}>`,
    to: data[0].email,
    bcc: process.env.MAIL,
    subject: 'Password Reset',
    html: `There was recently a request to change the password for your account. If you requested this password change, click the link below to reset your password:
  <br><br><a href="${process.env.WEBSITE_URL}/reset-password?email=${data[0].email}&key=${cryptoKey}}">${process.env.WEBSITE_URL}/reset-password?email=${data[0].email}&key=${cryptoKey}}</a><br><br>

  If you did not make this request, you can ignore this message and your password will remain the same.`,
  })
  connection.query(
    `UPDATE users SET resetPassword=?,resetPasswordExpiry='?' WHERE id=?`,
    [cryptoKey, Date.now() + 3600000, data[0].id],
  )
}

// CHECK PASSWORD LINK
exports.verifyPasswordLink = async (req, res) => {
  if (!req.body.email || !req.body.key)
    return res.status(400).send('Missing required field!')
  const { email, key } = req.body
  const [
    data,
  ] = await connection.query(
    `SELECT id,email,resetPassword,resetPasswordExpiry FROM users WHERE email=? AND resetPassword=? AND resetPasswordExpiry>=?`,
    [email, key, Date.now()],
  )
  if (!data.length) return res.status(418).send('Invalid key!')
  res.status(200).send(`${data[0].id}`)
}

// RESET PASSWORD
exports.changePassword = (req, res) => {
  if (!req.body.email || !req.body.key || !req.body.password)
    return res.status(400).send('Missing required field!')
  const { email, key, password } = req.body
  bcrypt.hash(
    password,
    Math.floor(Math.random() * 9) + 1,
    async (e, password) => {
      const [
        data,
      ] = await connection.query(
        `SELECT email,resetPassword,resetPasswordExpiry FROM users WHERE email=? AND resetPassword=?`,
        [email, key, Date.now()],
      )
      if (!data.length) return res.status(418).send('Invalid key!')
      connection.query(
        `UPDATE users SET password=?, resetPassword='' WHERE email=? AND resetPassword=?`,
        [password, email, key, Date.now()],
        () => res.sendStatus(200),
      )
    },
  )
}
